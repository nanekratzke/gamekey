# Gamekey

A reference REST service to store game states. 
This service is intended as a reference service used in Webtechnology Projects 
at Lübeck University of Applied Sciences.

## Installation via Docker (1st Option)

The most easiest way to deploy the gamekey reference service is via Docker.
You will find here [Docker](https://docs.docker.com/engine/installation/) installation instructions for various 
operating systems.

After installing docker you can fetch the latest gamekey image from Docker hub using this command.

    docker pull transit/gamekey


You can start the gamekey service like that

    docker run -v $(pwd):/data -p 8080:8080 transit/gamekey
    
This will fire up a container with a running gamekey service on port 8080 on your host.
Gamekey states will be stored in a gamekey.json file under your current working directory.
(Only if you are using the volume option -v). You can have a look into your gamekey state via

    cat gamekey.json
    
The docker installation provides only the server component in a handable manner.
The intended usage of this provisioning method is to run this service as a central server component.

## Installation via RubyGems (2nd Option)

To interact with this gamekey service you need a client. Gamekey is providing a command line client via RubyGems.org
for your convenience.

You will find here [Ruby](https://rvm.io/) installation instructions using rvm (Ruby Version Manager).
After you have installed rvm and Ruby 2.2 or higher successfully, you can install gamekey.

    $ gem install gamekey
    
You can get basic usage instructions via:

    $ gamekey help
    
If this is working, you have installed all gamekey components successfully.

## Usage

The gamekey package provides several basic commands to start and interact with the gamekey service.


### Start a gamekey service

    $ gamekey start
    
starts the gamekey service at port 8080.

    $ gamekey help start
    
provides additional usage instructions.

Another option is to start the gamekey service like shown in the Docker installation section.

### Register a game with the gamekey service

You have to register a game with the gamekey service to store gamestates.
To register a game you can use the register command like that.

    $ gamekey register --host http://192.168.99.100:8080 --name SnakeGame > snakegame.json
    
This will connect the gamekey service to register a game with name 'SnakeGame'.
The anser is stored as a JSON file. It should look like that:

    {
    "type": "game",
    "name": "SnakeGame",
    "id": "29bb0d8d-d3ad-4c43-8916-1e9b5d21f2b0",
    "url": "",
    "signature": "IZmfTtGDQrsVkZNjzL115toJIoEtpGyaCcrKdV852RQ=",
    "created": "2016-01-12T10:00:56.633213Z",
    "secret": "965c444369346f26"
    }

The command generated a random secret and unique id for the game.

__Be aware: The secret is only returned once! The secret is generated in the command line client
and not by the service itself. The gamekey service only stores
the signature, which can be only calculated by knowing id and secret. So if you are loosing your secret
your game has no access to the gamekey service any longer!__

Each game needs its _id_ and _secret_ for authentication and to do gamestate storing operations.
 
    $ gamekey help register
    
provides additional usage instructions. 

### Tests a gamekey service implementation

Your job is to analyze the gamekey service and reimplement it as a Dart service. Your game should work with
this reference implementation of the Gamekey Service but also with your Dart implementation.

You will find 

- REST API reference implementation in method [GamekeyService::api](lib/gamekey.rb)
- REST API reference testcases in class [GamekeyReferenceTests](lib/reference.rb)

The provided gamekey service is implemented in Ruby and is tested against a number of test cases.
You can fire the same test cases against your Dart implementation using this command here:

    $ gamekey test --host http://192.168.99.100:8080
    
Of course IP and port number should point at your Dart implementation and not at the Ruby reference implementation.

### Further commands

There are further commands to

- list all games
- update game data
- deregister games.

You get usage instructions via:

    $ gamekey help list
    $ gamekey help update
    $ gamekey help deregister

## Get the sources

Gamekey is provided via Bitbucket as a public git repository. 
So you can get all the sources via the following git command.

    $ git clone https://nanekratzke@bitbucket.org/nanekratzke/gamekey.git
    
## Changelog

### Version 0.0.4
Bugfix in reference test. Testcase PG-0 (checking that games could not be created without secrets).

### Version 0.0.3
POST /user checks now that a password and a user name is provided. 
Thanks again to Jan Lukas Deichmann figuring out this bug (WebTech SoSe 2016).

### Version 0.0.2
POST /game checks now that a secret is provided. 
Thanks to Jan Lukas Deichmann figuring out this bug (WebTech SoSe 2016).