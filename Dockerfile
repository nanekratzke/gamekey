FROM       ruby:2.2

COPY       . /usr/src/app
WORKDIR    /usr/src/app
RUN        bundle install
RUN        rake install

VOLUME     ["/data/"]

EXPOSE     8080

ENTRYPOINT ["gamekey", "start"]
CMD        ["--storage", "/data/gamekey.json"]