# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gamekey/version'

Gem::Specification.new do |spec|
  spec.name          = "gamekey"
  spec.version       = Gamekey::VERSION
  spec.authors       = ["Nane Kratzke"]
  spec.email         = ["nane.kratzke@fh-luebeck.de"]
  spec.description   = %q{Gamekey service.}
  spec.summary       = %q{Gamekey service.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.2.0'

  spec.add_runtime_dependency "sinatra"
  spec.add_runtime_dependency "sinatra-cross_origin"
  spec.add_runtime_dependency "commander"
  spec.add_runtime_dependency "colorize"
  spec.add_runtime_dependency "terminal-table"


  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
