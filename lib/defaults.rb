require "securerandom"
require "digest"
require "uri"

#
# Defines all CONSTANTS for Gamekey.
#
module Defaults

  # Default port number
  PORT    = 8080

  # Default storage file
  STORAGE = "gamekey.json"

  # Initial storage content
  DB = {
      service:    "Gamekey",
      storage:    SecureRandom.uuid,
      version:    "0.0.2",
      users:      [],
      games:      [],
      gamestates: []
  }

  # Used crpyto hash function
  CRYPTOHASH = Digest::SHA256.new

  # Default test host of gamekey service
  TESTHOST = "http://localhost:#{PORT}"

  # Regular Expression to validate Email adresses
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  # Regular Expression to validate http and https uris adresses
  VALID_URL_REGEX = /\A#{URI::regexp(['http', 'https'])}\z/

  # A Test hash for testing gamestate handling
  TESTHASH = {
      'this' => 'is',
      'a' => ['simple', 'test']
  }

  # A Test list for testing gamestate handling
  TESTLIST = ['This', 'is', 'a', 'Test']

end
