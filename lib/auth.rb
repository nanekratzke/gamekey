require 'defaults'
require 'base64'

module Auth

  def self.signature(id, pwd)
    Base64.strict_encode64(Defaults::CRYPTOHASH.digest("#{id},#{pwd}"))
  end

  def self.authentic?(entity, pwd)
    entity['signature'] == signature(entity['id'], pwd)
  end

end